<?php

	$crypted = @$_GET["crypted"];
	$save = @$_GET["save"];
	$download = @$_GET["download"];

	if (!isset($_GET["crypted"])){
		$crypted = "false";
	}

	if (!isset($_GET["save"])){
		$save = "true";
	}

	if (!isset($_GET["download"])){
		$download = "false";
	}

	$keys_to_hide = [
	    "calendar_next_calendars",
	    "calendar_today_calendars",
	    "wlanPass",
	    "owmApiKey",
	    "netatmo_access_token",
	    "netatmo_client_id",
	    "netatmo_client_secret",
	    "netatmo_refresh_token",
	    "spotify_client_secret",
	    "spotify_client_id",
	    "spotify_code",
	    "spotify_access_token",
	    "spotify_refresh_token",
	    "todoist_api_token",
	    "tanken_key",
	    "twitch_access_token",
	    "wunderlist_access_token",
	    "wunderlist_client_id",
	    "fetchsql_passwd",
	    "withings_consumer_key",
	    "withings_consumer_secret",
	    "withings_request_token_key",
	    "withings_request_token_secret",
	    "withings_access_key",
	    "withings_access_secret",
	    "withings_token",
	    "withings_test_token",
	    "soundcloud-info_clientkey",
	    "traffic_apiKey",
	    "traffic_destinationAddress",
	    "traffic_startAddress",
			"vbb_apiKey",
			"homeassistant_password"
	];

	$json_string = file_get_contents("../info.json");
	$json = json_decode($json_string, true);
	$version = $json["version"];

	$dump = [
		"version" => $version,
        "php_version" => phpversion(),
		"date" => date("Y-m-d H:i:s"),
		"crypted" => $crypted,
		"tutorial" => "https://glan.cr/import-export-dump",
		"user" => [
			"name" => "",
			"email" => ""
		],
		"keys" => [],
		"installed_modules" => []
	];

	include "../includes/db-connector.php";

	if ($result = $conn->query('SELECT * FROM configuration;')) {
	  while ($row = $result->fetch_assoc()) {
			if (in_array($row["key"], $keys_to_hide) && $crypted == "true"){
				$dump["keys"][$row["key"]] = key_crypteder($row["value"], round(strlen($row["value"])/6));
			} else {
				$dump["keys"][$row["key"]] = $row["value"];
			}

			if ($row["key"] == "firstname"){
				$dump["user"]["name"] = $row["value"];
			}

			if ($row["key"] == "email"){
				$dump["user"]["email"] = $row["value"];
			}

		}
	}

	$module_dir = "/var/www/html/modules/";

	$d = dir($module_dir);

	while (false !== ($module_name = $d->read())) {
		$info_file = $module_dir . $module_name. "/info.json";

		if ($module_name != "." && $module_name != ".." && file_exists($info_file)) {
			$module_info = json_decode(file_get_contents($info_file, true), true);

			array_push($dump["installed_modules"],
				[
					"name" => $module_name,
					"version" => $module_info["module"]["version"]
				]
			);
		}
	}

	$d->close();

	if (!file_exists('./backups')) {
	    mkdir('./backups', 0755, true);
	}

	$filename = "backups/mirros_dump_".time().".json";

	if ($save == "true"){
		$dump_file = fopen($filename, "w");
		fwrite($dump_file, json_encode($dump, JSON_PRETTY_PRINT));
		fclose($dump_file);
	}

	$url = "http://" . $_SERVER["HTTP_HOST"] . "/config/" . $filename;

	if ($download == "false"){
		header("Content-Type: application/json");
		echo json_encode($dump, JSON_PRETTY_PRINT);
	} else {
		header("Content-Type: application/octet-stream");
		header("Content-Transfer-Encoding: Binary");
		header("Content-disposition: attachment; filename=\"" . basename($url) . "\"");
		readfile($url);
		header("Connection: close");
	}

	function key_crypteder($key, $amount_chars) {
		$key_begin = substr($key, 0, round($amount_chars/2));
		$key_end = substr($key, round(0-$amount_chars/2), strlen($key));

		return $key_begin . "...". $key_end. " (Anzahl Zeichen: " . strlen($key) . ")";
	}

?>
