<?php

define('GLANCR_ROOT', substr(__DIR__, 0, strrpos(__DIR__, '/')));
//define('GLANCR_API_BASE', 'http://api.glancr.dev:8888');
define('GLANCR_API_BASE', 'https://api.glancr.de');

function getConfigValue($key) {

	include GLANCR_ROOT . "/includes/db-connector.php";

    /** @var mysqli $conn */
    $result = $conn->query('SELECT `value` FROM configuration WHERE `key`="' . $key . '"');

    if ($result->num_rows > 0) {
        $row = $result->fetch_assoc();
        return $row['value'];
    } else {
        return null;
    }
}

function setConfigValue($key, $value) {
	include GLANCR_ROOT . "/includes/db-connector.php";

    /** @var mysqli $conn */
    $conn->query("INSERT INTO configuration (`key`, `value`) VALUES ('" . $key . "', '" . $value . "') ON DUPLICATE KEY UPDATE value = '" . $value . "'");
}

function setGetTextDomain($domain, $directory) {
	bindtextdomain($domain, $directory);
	textdomain($domain);
	bind_textdomain_codeset($domain, 'UTF-8');
}

function getSystemInfo() {
    return json_decode(file_get_contents(GLANCR_ROOT . '/info.json'));
}

function dataProtectionStatementURL(bool $link) {
    $urlMap = [
        "de_DE" => "glancr.de/mirr-os/datenschutz",
        "en_GB" => "glancr.de/mirr-os/datenschutz"
    ];
    $localizedUrl = $urlMap[getConfigValue("language")] ?: "glancr.de/mirr-os/datenschutz";

    return $link === true ? "<a href=\"https://$localizedUrl\">$localizedUrl</a>" : $localizedUrl;
}

/**
 * Get all locales available in the mirr.OS installation. Does not check if the locale is available on the OS level.
 * @return array Installed locales that contain a valid MO file necessary for gettext translations.
 */
function getAvailableLocales() {
    $localesAvailable = [];
    foreach (new DirectoryIterator("../locale") as $fileInfo) {
        if($fileInfo->isDot() || $fileInfo->isFile()) continue;

        // Check if the directory contains the necessary MO file.
        $moFile = new SplFileInfo('../locale/' . $fileInfo->getFilename() . '/LC_MESSAGES/config.mo');
        if(!$moFile->isFile()) continue;

        $localesAvailable[] = $fileInfo->getFilename();
    }

    return $localesAvailable;
}