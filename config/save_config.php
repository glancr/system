<?php

	include('../config/glancrConfig.php');

	$setup = $_POST["setup"] === "true" ? "true" : "false";
	$language = $_POST['language'];
	$firstname = $_POST['firstname'];
	$email = $_POST['email'];
	$city = $_POST['cityId'];

	setConfigValue('connectionType', $_POST["connectionType"]);
	setConfigValue('firstname', $firstname);
	setConfigValue('language', $language);
	setConfigValue('email', $email);
	setConfigValue('city', $city);

	if (array_key_exists('connectionType', $_POST)){
		if ($_POST["connectionType"] == "wlan"){
			if (array_key_exists('invisibleWlan', $_POST) && array_key_exists('password', $_POST)) {
				if($_POST['invisibleWlan']) {
					if (array_key_exists('invisibleSsid', $_POST)) {
						setConfigValue('wlanName', trim($_POST['invisibleSsid']));
						setConfigValue('wlanInvisible', '1');
					} else {
						error_log("Error: network setting invisibleSsid missing");
					}
				} else {
					if (array_key_exists('ssid', $_POST)) {
						setConfigValue('wlanName', trim($_POST['ssid']));
						setConfigValue('wlanInvisible', '0');
					} else {
              error_log("Error: network setting ssid missing");
					}
				}
			} else {
          error_log("Error: network setting invisibleWlan or password missing");
			}
			setConfigValue('wlanPass', $_POST['password']);
		}
	} else {
      error_log("Error: network setting connectionType missing");
	}

	$language = getConfigValue('language');
	putenv("LANG=$language");
	setlocale(LC_ALL, $language . '.utf8');

	setGetTextDomain('config', GLANCR_ROOT ."/locale");
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
	<title><?php echo _('setup complete'); ?></title>
	<link rel="stylesheet" type="text/css" href="../config/css/main.css">
	<link rel="stylesheet" href="/config/bower_components/foundation-icon-fonts/foundation-icons.css" media="screen" title="no title" charset="utf-8">
	<script type="text/javascript" src="../config/bower_components/jquery/dist/jquery.min.js"></script>
	<script type="text/javascript" src="../config/bower_components/foundation-sites/dist/foundation.min.js"></script>
	<?php include "../includes/favicons.php"; ?>
</head>
<body>

	<?php include "../includes/navigation.php"; ?>

	<main class="container">
		<section class="row">
			<div class="small-12 columns">
				<p class="instruction__stepper">&nbsp;</p>
				<h2 class="instruction__title"><?php echo _('done');?></h2>
				<p class="instruction__text">
					<?php echo _('setup complete text'); ?>
				</p>
			</div>
		</section>
	</main>

	<script type="text/javascript">
		window.setTimeout(function() {
			window.location = "/wlanconfig/activatewlan.php?setup=<?php echo "$setup"; ?>";
		}, 5000);
	</script>
</body>
</html>
