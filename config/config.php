<?php
include('glancrConfig.php');

$setup = @$_GET["setup"];
$lang = @isset($_GET["lang"]);

if (!isset($_GET["setup"])){
	$setup = false;
}

if ($lang){
	setConfigValue("language", $_GET["lang"]);
}

if(isset($_POST['ssid'])) {
	$mywlan[0] = $_POST['invisibleWlan'];
	if($mywlan[0]) {
		$mywlan[1] = $_POST['invisibleSsid'];
	} else {
		$mywlan[1] = $_POST['ssid'];
	}
	$mywlan[2] = $_POST['pass'];

	$con[0] = $_POST['connectionType'];
} else {
	$mywlan[0] = getConfigValue('wlanInvisible');
	$mywlan[1] = getConfigValue('wlanName');
	$mywlan[2] = getConfigValue('wlanPass');
	$con[0] = getConfigValue('connectionType');
}

$email = getConfigValue('email');
$language = getConfigValue('language');
$firstname = getConfigValue('firstname');
$cityId = getConfigValue('city');

include "../includes/db-connector.php";

$sql = 'SELECT name FROM owm_cities WHERE id="' . $cityId . '"';

$result = $conn->query($sql);
if ($result->num_rows > 0) {
	$row = $result->fetch_assoc();
} else {
	$row['name'] = "";
}

$city = $row['name'];

$conn->close();

putenv("LANG=$language");
setlocale(LC_ALL, "$language.UTF-8");
bindtextdomain('config', GLANCR_ROOT ."/locale");
textdomain('config');
bind_textdomain_codeset('config', 'UTF-8');

$modules_content = scandir(GLANCR_ROOT .'/modules');
$modules_available = [];

$languagesAvailable = getAvailableLocales();

?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
	<title>Einstellungen</title>
	<link rel="stylesheet" type="text/css" href="/config/css/main.css">
	<link rel="stylesheet" href="/config/bower_components/foundation-icon-fonts/foundation-icons.css" media="screen" title="no title" charset="utf-8">
	<link rel="stylesheet" type="text/css" href="/config/bower_components/jquery/dist/jquery-ui.min.css">
    <link rel="stylesheet" type="text/css" href="css/draggable-upload.css">
	<script type="text/javascript" src="/config/bower_components/jquery/dist/jquery.min.js"></script>
	<script type="text/javascript" src="/config/bower_components/jquery/dist/jquery-ui.min.js"></script>
	<script type="text/javascript" src="/config/bower_components/jquery/dist/jquery.ui.autocomplete.html.js"></script>
	<script type="text/javascript" src="/config/bower_components/foundation-sites/dist/foundation.min.js"></script>
	<script type="text/javascript" src="js/animateRotate.js"></script>

	<?php include "../includes/favicons.php"; ?>

	<style type="text/css">
	.error {
		border: 2px solid red;
	}

	.validate {
		position: relative;
		top: -19px;
		font-size: small;
		color: #999;
		left: 2px;
		display: none;
	}

	.form-row input, .form-row select, .form-row a, .form-row label {
		width: 49%;
		display: inline-block;
	}

    /* Override for first setup form */
    #section_setup .form-row {
        display: flex;
        justify-content: space-around;
    }

    #section_setup .form-row button {
        max-width: 49%;
    }

	#config_file {
		width: 0.1px;
		height: 0.1px;
		opacity: 0;
		overflow: hidden;
		position: absolute;
		z-index: -1;
	}

	#config_file + label {
		cursor: pointer;
	}

	label[for='config_file'] {
		text-overflow: ellipsis;
	}

  label {
    display: inline;
    color: white;
  }

	#config_file + label * {
		pointer-events: none;
	}

	#section_setup a.active {
		background-color: #455b7a;
	}

	.language_container img {
		margin-right: 10px;
		margin-bottom: 10px;
	}

	.language_container img:hover {
		-moz-box-shadow: 0 0 5px #000;
		-webkit-box-shadow: 0 0 5px #000;
		box-shadow: 0 0 5px #000;
	}



	</style>
</head>
<body>

	<?php include "../includes/navigation.php"; ?>

	<main class="container">
		<section>
			<div class="row">
				<div class="small-12 columns">

					<section id='section_language'>
						<h3 class="instruction__title">Please select your language <br><small>Bitte wähle deine Sprache</small></h3><br>
						<div class="language_container">
							<?php
								if (!$lang) {
									if ($setup){
										echo "<select id='language_setup' name='language' style='float: right'>";
											echo "<optgroup label='" . _("your language") . "'></optgroup>";
												foreach ($languagesAvailable as $languageAvailable) {
													$selected = $languageAvailable == $language ? 'selected' : '';
													echo '<option class="languageItem" value="' . $languageAvailable . '" ' . $selected . '>' . Locale::getDisplayLanguage($languageAvailable, $languageAvailable) . "</option>\n";
										}
									echo "</select>";
									}
								}
							?>
                            <button id="language_setup_continue" type="button" class="button"><?php echo _('continue'); ?></button>
						</div>
					</section>

					<section id="section_setup">
						<?php
							if ($setup){
								echo "<h2 class='instruction__title'>Setup</h2>";
								echo "<div class='form-row'>";
									echo "<button class='button' id='config_new'><i class='fa fa-plus-circle'></i> " . _("setup mirros"). "</button>";
									echo "<button class='button' id='config_import'><i class='fa fa-upload'></i> " . _("restore mirros"). "</button>";
								echo "</div><br>";
							} else {
								echo "<h2 class='instruction__title'>" . _("settings") . "</h2>";
							}
						?>
					</section>



						<section id="section_import">
							<h4 class="instruction__title"><?php echo _("import existing settings"); ?> <?php if ($setup == false){ echo "/ " . _("export settings"); } ?></h4>
							<p class="instruction__text"><?php echo _("import settings text"); ?></p>

							<div class="form-row">
								<form id="restore_form" action="/config/import_config.php" method="post" enctype="multipart/form-data">
                                    <input id="config_file" type="file" name="config_file" accept=".json, application/json">
                                    <label for="config_file" class="button"><span><i class='fa fa-download'></i> <?php echo _("import settings"); ?></span></label>
									<?php if ($setup == false){ echo "<a href='/config/export_config.php?download=true' class='button' id='download_config' target='_blank' style='float: right'><i class='fa fa-upload'></i>" . _("export settings") . "</a>"; } ?>
								</form>
							</div>

							<div class="form-row">
								<button type="submit" onclick="submit_import_form()" class="button" id="import_settings_button" style="width: 100%"><i class="fa fa-play"></i> <?php echo _("import submit"); ?></button>
							</div><br><br>
						</section>

						<section id="section_new">
							<form id="setup_form" action="/config/save_config.php" method="post">

								<?php
									if ($setup){
										echo "<h3 class='instruction__title'>" . _("settings") . "</h3>";
									} else {
										echo "<h3 class='instruction__title'>" . _("edit settings") . "</h3>";
									}
								?>

                <p><?php echo _("The following information is needed to run mirr.OS and saved only on your local device. However, to send you e-mail notifications during setup or for updates, some information like your e-mail address and name are sent to our server to generate the e-mails. Nothing is stored permanently. For details, see our privacy statement at") . " " . dataProtectionStatementURL($link = true); ?></p>
                <input type="checkbox" required aria-required="true" name="privacy-consent" id="privacy-consent" />
                <label for="privacy-consent"><?php echo _("I agree"); ?></label>
                <hr>

								<div class="form-row">
									<input id="firstname" name="firstname" type="text" value="<?php echo $firstname ?>" placeholder="<?php echo _('what is your first name?')?>">
									<input id="city" name="city" type="text" placeholder="<?php echo _('where are you living?');?>" value="<?php echo $city; ?>" style="float: right">
									<input type="hidden" id="cityId" name="cityId" value="<?php echo $cityId;?>"/>
								</div>

								<div class="form-row">
									<?php
											echo "<select id='language' name='language' style='float: right'>";
												echo "<optgroup label='" . _("your language") . "'></optgroup>";
													foreach ($languagesAvailable as $languageAvailable) {
                                                        $selected = $languageAvailable == $language ? 'selected' : '';
                                                        echo '<option class="languageItem" value="' . $languageAvailable . '" ' . $selected . '>' . Locale::getDisplayLanguage($languageAvailable, $languageAvailable) . "</option>\n";
										}
										echo "</select>";
									?>
									<input id="email" name="email" type="email" placeholder="<?php echo _('your email address');?>" value="<?php echo $email; ?>" <?php if ($setup == true) { echo"style='width: 100%'"; } ?>>
								</div>

								<div class="form-row">
									<select id="connectionType" name="connectionType">
										<optgroup label="<?php echo _('connectionType'); ?>"></optgroup>
										<option value="wlan"><?php echo _('wireless');?></option>
										<option value="eth"><?php echo _('ethernet');?></option>
									</select>
									<select id="invisibleWlan" name="invisibleWlan" style="float: right">
										<optgroup label="<?php echo _('visibility'); ?>"></optgroup>
										<option value="0"><?php echo _('visible ssid');?></option>
										<option value="1"><?php echo _('invisible ssid');?></option>
									</select>
								</div>

								<div class="form-row">
									<select id="ssid" name="ssid" style="width: calc(100% - 55px) !important; margin-top: 12px">
										<option value=""><?php echo _('choose wlan') ?></option>
										<?php
											$wlans = file('../wlans.txt');
											$wlansListed = [];
											foreach($wlans as $wlan) {
												if(in_array($wlan, $wlansListed)) {
													break;
												}
												$wlansListed[] = $wlan;

												if(trim($wlan) == trim($mywlan[1])) {
													echo "<option value='" . $wlan . "' selected>$wlan</option>\n";
												} else {
													echo "<option value='" . $wlan . "'>$wlan</option>\n";
												}
											}
										?>
									</select>
									<a href="#" class="button" id="rescan" style="width: 40px; height: 39px; margin: 0 0 0 12px; text-align: center; "><i class='fa fa-refresh'></i></a>

								</div>

								<div class="form-row">
									<input id="invisibleSsid" name="invisibleSsid" type="text" placeholder="<?php echo _('invisible ssid');?>" value="<?php echo $mywlan[1];?>" style="display: none; width: 100%; margin-top: 12px;" autocorrect="off">
								</div>

								<div class="form-row">
									<input id="password" name="password" value="<?php if(isset($mywlan[2])) echo $mywlan[2];?>" type="password" style="width: calc(100% - 55px)" placeholder="<?php echo _('input password') ?>">
                                    <button data-toggle="#password" type="button" class="button" id="toggle-password" style="width: 40px; height: 39px; margin: 0 0 0 12px; text-align: center; "><i class='fa fa-eye field-icon toggle-password'></i></button>
								</div>

								<div class="form-row">
									<input id="setup" name="setup" type="hidden" value="<?php echo $setup; ?>" >

									<?php

										if ($setup){
											echo "<button onclick='setupSettings()' class='button' id='save' style='width: 100%'><i class='fa fa-arrow-right'></i> ". _("setup mirros") . "</button>";
										} else {
											echo "<button onclick='saveSettings()' class='button' id='save' style='width: 100%'><i class='fa fa-save'></i> ". _("save settings") ."</button>";
										}
									?>

									<br><br>

									<section id="reset">
										<h3 class='instruction__title'><?php echo _('factory settings');?></h3>
										<p class="instruction__text"><?php echo _('reset mirros text');?></p>
										<a href="/reset" class="button" style="width: 100%"><i class="fa fa-ban"></i> <?php echo _('reset mirros button');?></a>
									</section>

								</div>
							</form>
						</section>

				</div>
			</div>
		</section>
	</main>

	<script type="text/javascript">

		$("#connectionType").change(function(){
			if ($("#connectionType").val() == "eth"){
				$("#invisibleWlan").hide();
				$("#ssid").hide();
				$("#invisibleSsid").hide();
				$("#password").hide();
				$("#rescan").hide();
				$("#toggle-password").hide();
			} else {
				$("#ssid").show();
				$("#password").show();
                $("#toggle-password").show();
				$("#invisibleWlan").show();
				$("#rescan").show();
				$("#invisibleWlan").trigger('change');
            }
		});

		$("#invisibleWlan").change(function(){
			if ($("#invisibleWlan").val() == "1"){
				$("#ssid").hide();
				$("#invisibleSsid").show();
				$("#rescan").hide();
			} else {
				$("#invisibleSsid").hide();
				$("#ssid").show();
				$("#rescan").show();
			}
		});

		$("#config_import").click(function(){
			$("#section_new").hide();
			$("#section_import").fadeToggle( "slow", "linear" );
			$("#config_import").toggleClass("active");
			$("#config_new").removeClass("active");
		});

		$("#config_new").click(function(){
			$("#section_import").hide();
			$("#section_new").fadeToggle( "slow", "linear" );
			$("#config_new").toggleClass("active");
			$("#config_import").removeClass("active");
		});

		$('input[type=file]').change(function(e){
			path = $(this).val();
			splits = path.split("\\");
			filename = splits[splits.length-1];
		  $(this).next().html(filename);
		});

		$("#rescan").click(function(event) {
			event.preventDefault();
			$("#rescan .fa").addClass("fa-spin");
			$("#ssid").empty();
			$.getJSON("/wlanconfig/rescan.php", function(wlans) {

				wlanSelect = "";
				wlansListed = [];
				$("#ssid").append("<option value=''>" + "<?php echo _('choose wlan') ?>" +"</option>")
				$.each(wlans, function(key, value) {
					if(wlansListed.indexOf(value) == -1 && value.length) {
						wlansListed.push(value);
						if("<?php echo $mywlan[1];?>" == value) {
							$("#ssid").append("<option value='" + value + "' selected>" + value + "</option>");
						} else {
							$("#ssid").append("<option value='" + value + "'>" + value + "</option>");
						}
					}
				});
				setTimeout(function() {	$("#rescan .fa").removeClass("fa-spin"); }, 3000)
			});
		});

        /**
         * Creates a drag&drop-enabled box for easier restore file upload.
         */
        function showUploadDropzone() {
          let $form		 = $( '#restore_form' ),
            $label		 = $form.find( 'label' ),
            droppedFiles = false,
            showFiles	 = function( files )
            {
              $label.text().replace( files[0].name );
            };
          // Test if we are in the macOS Captive Network Assistant
          if (window.navigator.userAgent.includes("Macintosh") && window.navigator.userAgent.includes("AppleWebKit") &&
            !window.navigator.userAgent.includes("Safari") && !window.navigator.userAgent.includes("Chrome")) {
            $('#config_file').addClass('mac_cna');
            $('#config_file').before("<p style='color: #de0a0a'><?php echo htmlspecialchars(_('The macOS network assistant currently does not support file selection, but you can upload your mirr.OS settings export file by dragging it on the box below (drag & drop).')); ?></p>");
          }
          // Event handlers for drag & drop.
          $form
            .addClass( 'has-advanced-upload' ) // letting the CSS part to know drag&drop is supported by the browser
            .on( 'drag dragstart dragend dragover dragenter dragleave drop', function( e )
            {
              // preventing the unwanted behaviours
              e.preventDefault();
              e.stopPropagation();
            })
            .on( 'dragover dragenter', function() //
            {
              $form.addClass( 'is-dragover' );
            })
            .on( 'dragleave dragend drop', function()
            {
              $form.removeClass( 'is-dragover' );
            })
            .on( 'drop', function( e )
            {
              droppedFiles = e.originalEvent.dataTransfer.files; // the files that were dropped
              showFiles( droppedFiles ); // Show the file name on the drop box
              document.querySelector('#config_file').files = droppedFiles;
            });
        }

		function validateForm(){
			error = false;
			$(".error").removeClass("error");

			info_check = $("#firstname").val() != "" && $("#city").val() != "" && $("#email").val() != "";
			connection_type_check = $("#connectionType").val() != "";
			wlan = $("#connectionType").val() == "wlan";
			visible_wlan = $("#invisibleWlan").val() == "0"
			visible_ssid_check =  $("#ssid").val() != "";
			invisible_ssid_check = $("#invisibleSsid").val() != "";
			password_check = $("#password").val() != "";
			privacy_consent_check = $('#privacy-consent')[0].checked

      if (!privacy_consent_check) {
        error = true;
			  $("#firstname").addClass("error");
      }

			if (info_check) {
				if (connection_type_check) {
					if (wlan) {
						if (visible_wlan) {
							if (!visible_ssid_check){
								error = true;
								$("#ssid").addClass("error");
							}
						} else {
							if (!invisible_ssid_check){
								error = true;
								$("#invisibleSsid").addClass("error");
							}
						}
						if (!password_check){
							error = true;
							$("#password").addClass("error");
						}
					}
				} else {
					error = true;
					$("#connectionType").addClass("error");
				}
			} else {
				error = true;
				$("#firstname").addClass("error");
				$("#city").addClass("error");
				$("#email").addClass("error");
			}
			return error
		}

		function submit_import_form() {
			if ($("#config_file").val() == "") {
				alert("<?php echo _('no file selected'); ?>");
			} else {
				$('#restore_form').submit();
			}
		}

		function setupSettings() {

			error = validateForm();

			if (error){
				$('#error').show(30, function() {
					$(this).hide('slow');
				});
			} else {
				$("#setup_form").submit();
			}
		}

		function saveSettings() {
			error = validateForm();

			if (error){
				$('#error').show(30, function() {
					$(this).hide('slow');
				});
			} else {
				$("#setup_form").submit();
			}
		}

        // Test if we are on a touch-based device.
        function is_touch_device() {
          return 'ontouchstart' in window        // works on most browsers
            || navigator.maxTouchPoints;       // works on IE10/11 and Surface
        };

		$(document).ready(function() {
			<?php
				if ($setup && $lang){
					echo"
						$('#section_import').hide();
						$('#section_new').hide();
						$('#section_language').hide();
						$('#language').hide();
						$('#reset').hide();
						if (!is_touch_device()) {
                            $('#restore_form label').removeClass('button');
                            $('#restore_form').attr('style', '').addClass('box');
                            showUploadDropzone();
                        }";
				}

				if ($lang){
					echo "$('#section_language').hide();";
				}

				if (!$lang && $setup) {
					echo"
						$('#section_setup').hide();
						$('#section_import').hide();
						$('#section_new').hide();";
				}

				if (!$lang && !$setup) {
					echo "$('#section_language').hide();";
				}

				if($con[0] == 'eth') {
					echo "$('#connectionType').trigger('change');";
				}
			?>

			$("#connectionType").val("<?php echo trim($con[0])?>");
			$("#invisibleWlan").val(<?php echo trim($mywlan[0])?>);
			$("#invisibleWlan").trigger("change");
			$("#connectionType").trigger("change");


			if ($("#connectionType").val() == null) {
				$("#connectionType").val("wlan");
			}
		});

		$("#city").autocomplete({
			source: "/wlanconfig/autocompleteCities.php",
			html: true,
			minLength: 2,
			change: function(event, ui) {
				if(ui.item) {
					$(this).val(ui.item.value);
					$('#cityId').val(ui.item.id);
				} else {
					$(this).val("");
					$('#cityId').val("");
				}
			}
		});

		$("#language").change(function(event) {
			window.location = "/config/config.php?lang="+$("#language").val();
		});

		$("#language_setup").change(function(event) {
			window.location = "/config/config.php?setup=true&lang="+$("#language_setup").val();
		});

		$("#language_setup_continue").click(function(event) {
          window.location = "/config/config.php?setup=true&lang="+$("#language_setup").val();
        });

        $("#toggle-password").click(function() {

          $(this).children("i").toggleClass("fa-eye fa-eye-slash");
          var input = $($(this).attr("data-toggle"));
          if (input.attr("type") === "password") {
            input.attr("type", "text");
          } else {
            input.attr("type", "password");
          }
        });

	</script>
</body>
</html>
