<?php

	include 'glancrConfig.php';

	$language = getConfigValue('language');
	putenv("LANG=$language");
	setlocale(LC_ALL, $language . '.utf8');

	setGetTextDomain('config', GLANCR_ROOT ."/locale");

	// Backup curret data
	file_get_contents("http://localhost/config/export_config.php?save=true");

	$target_dir = "./backups/restored/";

	$message = "";
	$error = "";

	$target_file = $target_dir . time() . "_" . basename($_FILES["config_file"]["name"]);
	$uploadOk = true;
	$imageFileType = pathinfo($target_file, PATHINFO_EXTENSION);

	if (file_exists($target_file)) {
	  $error .= "Error: " . _("file already exists") . "<br>";
	  $uploadOk = false;
	}

	if($imageFileType != "json") {
	  $error .= "Error: " . _("only mirros backup file") . "<br>";
	  $uploadOk = false;
	}

	if ($uploadOk == false) {
	  $error .= "Error: " . _("not uploaded") . "<br>";
	} else {
	  if (move_uploaded_file($_FILES["config_file"]["tmp_name"], $target_file). time()) {

			$string = file_get_contents($target_file);
			$json = json_decode($string,true);

			if ($json["crypted"] == "false"){
				foreach ($json["keys"] as $key => $value) {
					if ($key != "ip"){
						setConfigValue($key, $value);
					}
				}
				$message .= _("restored from file") . "<br> '". basename( $_FILES["config_file"]["name"]). "'";
			} else {
				$error .=  "Error: " . _("file crypted") . "<br>";
			}
	  } else {
	  	$error .=  "Error: " . _("file not moved") . "<br>";
	  }
	}

?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
	<title><?php echo _('setup complete'); ?></title>
	<link rel="stylesheet" type="text/css" href="../config/css/main.css">
	<link rel="stylesheet" href="/config/bower_components/foundation-icon-fonts/foundation-icons.css" media="screen" title="no title" charset="utf-8">
	<script type="text/javascript" src="../config/bower_components/jquery/dist/jquery.min.js"></script>
	<script type="text/javascript" src="../config/bower_components/foundation-sites/dist/foundation.min.js"></script>
	<?php include "../includes/favicons.php"; ?>
</head>
<body>

	<?php include "../includes/navigation.php"; ?>

	<main class="container">
		<section class="row">
			<div class="small-12 columns">
				<p class="instruction__stepper">&nbsp;</p>
				<h2 class="instruction__title">Import</h2>
				<p class="instruction__text">
					<?php echo $message; ?>
					<?php echo $error; ?><br><br>

					<?php
						if ($error == ""){
							echo _('setup complete text');
						}
					?>
				</p>
			</div>
		</section>
	</main>


	<?php if ($error == ""){ ?>
		<script type="text/javascript">
			window.setTimeout(function() {
				window.location = "/wlanconfig/activatewlan.php";
			}, 5000);
		</script>
	<?php } ?>

	<?php if ($message == ""){ ?>
		<script type="text/javascript">
			window.setTimeout(function() {
				window.location = "/config/config.php?setup=true&lang=<?php echo $language; ?>";
			}, 5000);
		</script>
	<?php } ?>

</body>
</html>
