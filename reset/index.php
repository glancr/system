<?php
include('../config/glancrConfig.php');

$language = getConfigValue('language');
$ip = getConfigValue('ip');
putenv("LANG=$language");
setlocale(LC_ALL, $language . '.utf8');

setGetTextDomain('config', GLANCR_ROOT . "/locale");
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
    <title><?php echo _('factory settings'); ?></title>
    <link rel="stylesheet" type="text/css" href="../config/css/main.css">
    <link rel="stylesheet" href="../config/bower_components/foundation-icon-fonts/foundation-icons.css" media="screen"
          title="no title" charset="utf-8">
    <script type="text/javascript" src="../config/bower_components/jquery/dist/jquery.min.js"></script>
    <script type="text/javascript" src="../config/bower_components/foundation-sites/dist/foundation.min.js"></script>
    <?php include "../includes/favicons.php"; ?>
    <style type="text/css">
        .working::after {
            content: '…';
            display: inline;
        }
    </style>
</head>
<body>

<?php include "../includes/navigation.php"; ?>

<main class="container">
    <section class="row">
        <div class="small-12 columns">
            <h2 class="instruction__title"><?php echo _('factory settings'); ?></h2>
            <p class="instruction__text">
                <?php echo _('factory setting text'); ?>
            </p>
            <a class="button" style="width: 100%" href="doreset.php" onclick="handleSubmit(event)"><i
                    class="fa fa-ban"></i> <?php echo _('reset mirros button'); ?></a>
        </div>
    </section>
</main>

<script type="text/javascript">
    function handleSubmit (e) {
        e.preventDefault()

        // Provide visual feedback that the reset is in progress.
        e.target.setAttribute('disabled', true)
        e.target.classList.add('working')
        $(e.target).children('i.fa').replaceWith('<i class="fa fa-spinner"></i>')

        // Request doreset.php to perform the actual request. Set a timeout of 10 seconds to account for Pi Zero W speeds.
        let req = new XMLHttpRequest()
        req.open('GET', e.target.getAttribute('href'))
        req.timeout = 20000

        // Triggering a reset will drop glancr from the local network, so requesting doreset.php should result in timeout.
        req.ontimeout = function () {

            // doreset.php didn't answer, reset probably went through. Get configuration page to see if it's still available.
            let req = new XMLHttpRequest()
            req.open('GET', '../config/')
            req.timeout = 20000

            req.ontimeout = function () {
                // Configuration page no longer available, reset went through.
                handleSuccessfulReset()
            }

            req.onload = function () {
                // Configuration page still available, reset probably failed. Redirect to config.
                handleFailedReset()
            }
            req.send()

        }
        // If doreset.php somehow answers the request before timeout, something must be wrong.
        req.onload = function () {
            handleFailedReset()
        }

        req.send()
    }

    function handleSuccessfulReset () {
        $('a[href="doreset.php"]').remove()
        $('.instruction__text').replaceWith('<p><?php echo _('Reset successful! This page will close in a few seconds, please follow the instructions that will appear on your Smart Mirror after the reset is complete.');?></p>')
        window.setTimeout(function () { window.open('', '_self').close() }, 15000)
    }

    function handleFailedReset () {
        $('a[href="doreset.php"]').remove();
        $('.instruction__text').replaceWith('<p><i class="fa fa-exclamation-triangle"></i><?php echo _('Reset failed! You will be redirected to the configuration page, please try again and send us a bug report if this keeps happening.');?></p>')
        window.setTimeout(function () { window.location = '../'}, 15000)
    }

</script>

</body>
</html>
